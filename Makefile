
CHART_REPO=chinchillas

push-chart: check-app
	@echo "Pushing chart charts/${APP}"
	helm cm-push --debug charts/${APP} ${CHART_REPO}
.PHONY: push-chart

check-app:
ifndef APP
	$(error APP is undefined)
endif
.PHONY: check-app
